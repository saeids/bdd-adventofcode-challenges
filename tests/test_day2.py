import pytest
from day_2_part_1 import validCount as day2_part1_result
from day_2_part_2 import validCount as day2_part2_result

# Your test cases for day 2 should be defined here

# Example test case for day 2 (Part 1)
def test_day2_part1_example():
    expected_result = 434
    assert day2_part1_result == expected_result

# Example test case for day 2 (Part 2)
def test_day2_part2_example():
    expected_result = 509
    assert day2_part2_result == expected_result

# You can add more test cases for day 2 (Part 1) and day 2 (Part 2) as needed.
