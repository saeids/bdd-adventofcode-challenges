
**Advent of Code Project**

This repository contains my solutions to Advent of Code challenges from a specific year, approached using Behavior-Driven Development (BDD) practices and pytest-bdd.

**Overview**

- Challenges: I completed at least the first 2 days of Advent of Code, totaling 4 challenges.
- Testing Approach: I used BDD practices and pytest-bdd for testing.
- Code Coverage: I aimed for comprehensive code coverage to ensure thorough testing.

**How to Run**

1-Clone the repository.

2-Navigate to the project directory.

3-Install dependencies: pip install -r requirements.txt.

4-Run tests and generate a coverage report: pytest --verbose tests/.

**Reflections**

Working with BDD allowed for clear and executable specifications of challenge requirements, improving understanding and communication. BDD was effective for translating challenges into scenarios.
