import csv

# Part 1
with open('input2.csv') as data:
    reader = csv.reader(data, delimiter=' ')

    validCount = 0
    for row in reader:
        quota, letter, pw = row[0], row[1][0], row[2]

        # Get the range
        # 'a-b'
        i = quota.index('-')
        lower = int(quota[:i])
        upper = int(quota[i+1:])

        count = 0
        for character in pw:
            if character == letter:
                count += 1

        if count >= lower and count <= upper:
            validCount += 1

print(validCount)
