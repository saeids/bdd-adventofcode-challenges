def find_product_of_two_numbers(nums, target):
    largest = -1

    for i in range(len(nums) - 1):
        for j in range(i + 1, len(nums)):
            num1 = nums[i]
            num2 = nums[j]

            if num1 + num2 == target:
                product = num1 * num2

                if product > largest:
                    largest = product

    return largest

def main():
    try:
        with open('input1.txt') as f:
            lines = f.readlines()

        nums = [int(line.strip()) for line in lines]
        target = 2020

        result = find_product_of_two_numbers(nums, target)
        print(f'The largest product of two numbers that sum up to {target} is: {result}')
    except FileNotFoundError:
        print("Error: 'input1.txt' file not found.")
    except Exception as e:
        print(f"An error occurred: {str(e)}")

if __name__ == "__main__":
    main()
